package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"math/rand"
	"os"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/docker/cli/cli/config/types"
	api "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	_ "k8s.io/client-go/plugin/pkg/client/auth"
	restclient "k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

const (
	registryUser  = "root"
	registryPass  = "****"
	registryURL   = "cat.registry:5050"
	buildImage    = "cat.registry:5050/root/repro-k8s:latest"
	helperImage   = "cat.registry:5050/root/repro-k8s/helper:x86_64-a9b62539"
	kubeNamespace = "pulltests"
)

var (
	uniqueName string
	creds      *api.Secret
	configMap  *api.ConfigMap
	kubeConfig *restclient.Config
	kubeClient *kubernetes.Clientset
	genPod     *api.Pod
)

func setUniqueName() {
	rand.Seed(time.Now().UnixNano())
	uniqueName = fmt.Sprintf(
		"runner-project-42-concurrent-%d-",
		rand.Intn(99999),
	)
}

func getDefaultKubernetesConfig() (*restclient.Config, error) {
	config, err := clientcmd.NewDefaultClientConfigLoadingRules().Load()
	if err != nil {
		return nil, err
	}

	return clientcmd.NewDefaultClientConfig(*config, &clientcmd.ConfigOverrides{}).ClientConfig()
}

func initKubernetes() (err error) {
	kubeConfig, err = getDefaultKubernetesConfig()
	if err != nil {
		return err
	}

	kubeConfig.UserAgent = "gitlab-runner-repro-test"

	kubeClient, err = kubernetes.NewForConfig(kubeConfig)
	return err
}

func setupCredentials() (err error) {
	logger := log.New()
	logger.Info("Setting up secrets")

	dockerCfgs := make(map[string]types.AuthConfig)
	dockerCfgs[registryURL] = types.AuthConfig{
		Username:      registryUser,
		Password:      registryPass,
		ServerAddress: registryURL,
	}

	dockerCfgContent, err2 := json.Marshal(dockerCfgs)
	if err2 != nil {
		return err2
	}

	logger.WithFields(log.Fields{"cfg_json": dockerCfgContent}).Debug("Generated config")

	secret := api.Secret{}
	secret.GenerateName = uniqueName
	secret.Namespace = kubeNamespace
	secret.Type = api.SecretTypeDockercfg
	secret.Data = map[string][]byte{}
	secret.Data[api.DockerConfigKey] = dockerCfgContent

	creds, err = kubeClient.CoreV1().Secrets(kubeNamespace).Create(&secret)
	return err
}

func setupScriptsConfigMap() (err error) {
	logger := log.New()
	logger.Info("Setting up scripts configmap")

	configMap1 := &api.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			GenerateName: fmt.Sprintf("%s-scripts", uniqueName),
			Namespace:    kubeNamespace,
		},
		Data: nil,
	}

	configMap, err = kubeClient.CoreV1().ConfigMaps(kubeNamespace).Create(configMap1)
	return err
}

func buildContainer(
	name, image string,
	containerCommand ...string,
) api.Container {
	privileged := false
	allowPrivilegeEscalation := false

	return api.Container{
		Name:            name,
		Image:           image,
		ImagePullPolicy: api.PullPolicy("Always"),
		Command:         containerCommand,
		Args:            make([]string, 0),
		SecurityContext: &api.SecurityContext{
			Privileged:               &privileged,
			AllowPrivilegeEscalation: &allowPrivilegeEscalation,
		},
		Stdin: true,
	}
}


func preparePodConfig(
	imagePullSecrets []api.LocalObjectReference,
	initContainers []api.Container,
) api.Pod {
	pod := api.Pod{
		ObjectMeta: metav1.ObjectMeta{
			GenerateName: uniqueName,
			Namespace:    kubeNamespace,
		},
		Spec: api.PodSpec{
			ServiceAccountName: "default",
			RestartPolicy:      api.RestartPolicyNever,
			InitContainers:     initContainers,
			Containers: append([]api.Container{
				buildContainer(
					"build",
					buildImage,
					[]string{"sh", "-c", "echo 'build'"}...,
				),
				buildContainer(
					"helper",
					helperImage,
					[]string{"sh", "-c", "echo 'helper'"}...,
				),
			}),
			ImagePullSecrets: imagePullSecrets,
		},
	}

	return pod
}

func setupBuildPod(initContainers []api.Container) (err error) {
	logger := log.New()
	logger.Info("Setting up build pod")

	var imagePullSecrets []api.LocalObjectReference
	imagePullSecrets = append(imagePullSecrets, api.LocalObjectReference{Name: creds.Name})

	podConfig := preparePodConfig(imagePullSecrets, initContainers)

	logger.Info("Creating build pod")
	genPod, err = kubeClient.CoreV1().Pods(kubeNamespace).Create(&podConfig)
	return err
}

func isRunning(pod *api.Pod) (bool, error) {
	switch pod.Status.Phase {
	case api.PodRunning:
		return true, nil
	case api.PodSucceeded:
		return true, nil
		// return false, fmt.Errorf("pod already succeeded before it begins running")
	case api.PodFailed:
		return false, fmt.Errorf("pod status is failed")
	default:
		return false, nil
	}
}


type podPhaseResponse struct {
	done  bool
	phase api.PodPhase
	err   error
}

func getPodPhase(pod *api.Pod) podPhaseResponse {
	logger := log.New()

	pod, err := kubeClient.CoreV1().Pods(pod.Namespace).Get(pod.Name, metav1.GetOptions{})
	if err != nil {
		return podPhaseResponse{true, api.PodUnknown, err}
	}

	ready, err := isRunning(pod)

	if err != nil {
		return podPhaseResponse{true, pod.Status.Phase, err}
	}

	if ready {
		return podPhaseResponse{true, pod.Status.Phase, nil}
	}

	// check status of containers
	for _, container := range pod.Status.ContainerStatuses {
		if container.Ready {
			continue
		}
		if container.State.Waiting == nil {
			continue
		}

		switch container.State.Waiting.Reason {
		case "ErrImagePull", "ImagePullBackOff", "InvalidImageName":
			err = fmt.Errorf("image pull failed: %s", container.State.Waiting.Message)
			return podPhaseResponse{true, api.PodUnknown, err}
		}
	}

	logger.WithFields(log.Fields{"namespace": pod.Namespace, "name": pod.Name, "phase": pod.Status.Phase}).
		Info("Waiting for pod to be running")

	for _, condition := range pod.Status.Conditions {
		// skip conditions with no reason, these are typically expected pod
		// conditions
		if condition.Reason == "" {
			continue
		}

		logger.WithFields(log.Fields{"reason": condition.Reason, "message": condition.Message}).
			Debug("encountered pod status condition")
	}

	return podPhaseResponse{false, pod.Status.Phase, nil}
}


func triggerPodPhaseCheck(pod *api.Pod) <-chan podPhaseResponse {
	errc := make(chan podPhaseResponse)
	go func() {
		defer close(errc)
		errc <- getPodPhase(pod)
	}()
	return errc
}

func waitForPodRunning(
	pod *api.Pod,
) (api.PodPhase, error) {
	pollInterval := 3
	pollTimeout := 180
	pollAttempts := pollTimeout / pollInterval

	for i := 0; i <= pollAttempts; i++ {
		select {
		case r := <-triggerPodPhaseCheck(pod):
			if !r.done {
				time.Sleep(time.Duration(pollInterval) * time.Second)
				continue
			}
			return r.phase, r.err
		}
	}
	return api.PodUnknown, errors.New("timed out waiting for pod to start")
}

func cleanupResources() {
	logger := log.New()

	err := kubeClient.CoreV1().Pods(genPod.Namespace).Delete(genPod.Name, &metav1.DeleteOptions{})
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("error cleaning up pod")
	}
	err = kubeClient.CoreV1().Secrets(kubeNamespace).Delete(creds.Name, &metav1.DeleteOptions{})
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("error cleaning up secrets")
	}
	err = kubeClient.CoreV1().ConfigMaps(kubeNamespace).Delete(configMap.Name, &metav1.DeleteOptions{})
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("error cleaning up configmap")
	}
}

func main() {
	setUniqueName()

	err := initKubernetes()
	if err != nil {
		log.WithFields(log.Fields{"error": err}).Error("initializing kubernetes")
	}

	err = setupCredentials()
	if err != nil {
		log.WithFields(log.Fields{"error": err}).Error("setting up credentials")
	}

	err = setupScriptsConfigMap()
	if err != nil {
		log.WithFields(log.Fields{"error": err}).Error("generating scripts configmap")
	}

	err = setupBuildPod([]api.Container{})
	if err != nil {
		log.WithFields(log.Fields{"error": err}).Error("setting up build pod")
	}

	successfulExitCode := 0
	_, err = waitForPodRunning(genPod)
	if err != nil {
		log.WithFields(log.Fields{"error": err}).Error("waiting for pod running")
		successfulExitCode = 1
	}

	log.WithFields(log.Fields{"pod": genPod.Name}).Info("pod is now running")

	if successfulExitCode == 0 {
	  cleanupResources()
	  log.Info("cleaned up")
	}

	os.Exit(successfulExitCode)
}
